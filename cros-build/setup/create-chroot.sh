#!/bin/bash

set -e

env

echo "device: $CROS_DEVICE"
chroot_dir=$HOME/chroot-"$CROS_DEVICE"
cache_dir=$HOME/cache

echo "Creating CrOS SDK chroot..."
cros_sdk \
    --enter \
    --nouse-image \
    --no-ns-pid \
    --debug \
    --chroot "$chroot_dir" \
    --cache-dir "$cache_dir" \
    $@

exit 0
