#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=nami

cmd_setup() {
    ./setup_board --board=$BOARD
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-sona-10775.108.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    emerge-${BOARD} depthcharge
}

cmd_image() {
    local output=firmware/sona-new.bin

    cp firmware/sona.bin ${output}
    cbfstool \
        ${output} \
        remove \
        -n fallback/payload \
        -r COREBOOT
    cbfstool \
        ${output} \
        add-payload \
        -r COREBOOT \
        -n fallback/payload \
        -f /build/${BOARD}/firmware/depthcharge/dev.elf

    ls -l ${output}
}

cmd_$1

exit 0
