#!/bin/bash

# Custom build script for 'kevin' ('gru').
#
# It will create the chroot into a 'build_kevin' directory and leave the
# final custom firmware binary in build_kevin/kevin.bin.
#
# In order for this to work you'll need a few requirements installed in
# the host (see the apt-get commands in
# firmware-tools/cros-build/setup/Dockerfile).

# Create directory structure and download the build tools locally.
mkdir -p build_kevin
cp setup/kevin.sh build_kevin
cd build_kevin
git clone --depth=1 \
        https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=depot_tools:$PATH

# Download the sources
repo init -u https://chromium.googlesource.com/chromiumos/manifest.git \
        -b firmware-gru-8785.B --depth=1
# The imageloader project can't be sync'd anymore
sed -i '/imageloader/d' .repo/manifests/full.xml
repo sync

ln -rs kevin.sh src/scripts
chmod +x src/scripts/kevin.sh

# Create the sdk
cros_sdk --enter --no-ns-pid --debug --chroot chroot --cache-dir cache

# Run the following once you're inside the sdk chroot:
#
# ./kevin.sh setup
# ./kevin.sh checkout
# ./kevin.sh build
#
# The resulting firmware will be in build_kevin/kevin.bin
