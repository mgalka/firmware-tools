#!/bin/bash

# Custom build script for 'hana' ('oak').
#
# It will create the chroot into a 'build_hana' directory and leave the
# final custom firmware binary in build_hana/hana.bin.
#
# In order for this to work you'll need a few requirements installed in
# the host (see the apt-get commands in
# firmware-tools/cros-build/setup/Dockerfile).

# Create directory structure and download the build tools locally.
mkdir -p build_hana
cp setup/hana.sh build_hana
cd build_hana
git clone --depth=1 \
        https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=depot_tools:$PATH

# Download the sources
repo init -u https://chromium.googlesource.com/chromiumos/manifest.git \
        -b firmware-oak-8438.B --depth=1
# The imageloader project can't be sync'd anymore
sed -i '/imageloader/d' .repo/manifests/full.xml
repo sync

ln -rs hana.sh src/scripts
chmod +x src/scripts/hana.sh

# Create the sdk
cros_sdk --enter --no-ns-pid --debug --chroot chroot --cache-dir cache

# Run the following once you're inside the sdk chroot:
#
# ./hana.sh setup
# ./hana.sh checkout
# ./hana.sh build
#
# The resulting firmware will be in build_hana/hana.bin
